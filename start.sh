#!/bin/bash

docker run -d --name selenium-vnc \
-p 4445:4444 -p 5901:5900 \
-v /dev/shm:/dev/shm \
selenium-vnc:latest
